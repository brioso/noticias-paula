import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage-angular';
import { Article } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class DataLocalService {

  favNews: Article[] = [];

  constructor(private storage: Storage) {
    this.storage.create();
  }

  guardarNew(noticia: Article) {
    this.favNews.unshift(noticia);
    this.storage.set('favs', this.favNews);
  }

  borrarNew(noticia: Article) {
    this.favNews = this.favNews.filter(noti => noti.title !== noticia.title);
    this.storage.set('favs', this.favNews);
  }

  async cargarFavs() {
    const favs = await this.storage.get('favs');

    if(favs) {
      this.favNews = favs;
    }
  }

  getNews() {
    return this.favNews;
  }
  
}
