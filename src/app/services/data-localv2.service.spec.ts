import { TestBed } from '@angular/core/testing';

import { DataLocalv2Service } from './data-localv2.service';

describe('DataLocalv2Service', () => {
  let service: DataLocalv2Service;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DataLocalv2Service);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
