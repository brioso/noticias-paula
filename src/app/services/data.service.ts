import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RootObject } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }

  getUsuarios() {
    return this.http.get('https://jsonplaceholder.typicode.com/users');
  }

  getNews() {
    return this.http.get<RootObject>('https://newsapi.org/v2/top-headlines?language=es&apiKey=df0240106af744dd8815ebc1694be8ec');
  }

  getNewFilter(cat: string) {
    var link = 'https://newsapi.org/v2/top-headlines?language=es&category=' + cat + '&apiKey=df0240106af744dd8815ebc1694be8ec';
    return this.http.get<RootObject>(link);
  }

  getBusinessNews() {
    return this.http.get<RootObject>('https://newsapi.org/v2/top-headlines?language=es&category=business&apiKey=df0240106af744dd8815ebc1694be8ec');
  }

  getGeneralNews() {
    return this.http.get<RootObject>('https://newsapi.org/v2/top-headlines?language=es&category=general&apiKey=df0240106af744dd8815ebc1694be8ec');
  }

  getEntertainmentNews() {
    return this.http.get<RootObject>('https://newsapi.org/v2/top-headlines?language=es&category=entertainment&apiKey=df0240106af744dd8815ebc1694be8ec');

  }
}
