import { Injectable } from '@angular/core';
import { Article } from '../interfaces/interfaces';
import { Storage } from '@ionic/storage-angular';

@Injectable({
  providedIn: 'root'
})
export class DataLocalv2Service {


  notis: Article[] = [];

  constructor(private storage: Storage) {
    this.storage.create();
  }


  guardarNew(noti: Article) {
    this.notis.unshift(noti);
    this.storage.set('favs', this.notis);
  }


  async cargarFavs() {
    const favs = await this.storage.get('favs');
    
    if (favs) {
      this.notis = favs;
    }
  }




}
