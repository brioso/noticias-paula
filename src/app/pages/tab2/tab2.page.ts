import { Component } from '@angular/core';
import { Plugins } from '@capacitor/core';
const { Browser } = Plugins;
import { DataService } from '../../services/data.service';
import { Article } from '../../interfaces/interfaces';
import { ActionSheetController } from '@ionic/angular';
import { DataLocalService } from '../../services/data-local.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

//segment;

  constructor(private datalocal: DataLocalService, private data: DataService, public actionSheetController: ActionSheetController) {
    this.loadBusinessNews();
    //this.segment = "General";
    
  }

  news: Array<Article> = [];

  loadNews(event?: any) {
    this.data.getNews().subscribe(
      resp => {
        //console.log('News', resp);

        this.news.push(...resp.articles);

        if (event) {
          event.target.complete();
        }
      }
    )
  }

  loadData(event) {
    this.loadNews(event);
  }

  ngOnInit(): void {
    this.loadNews();
  }



  async openBrowser(link: string) {
    await Browser.open({ url: link });
  }








  loadGeneralNews(event?: any) {
    this.news = [];

    this.data.getNewFilter('general').subscribe(
      resp => {

        this.news.push(...resp.articles);

        if (event) {
          event.target.complete();
        }
      }
    )
  }

  loadBusinessNews(event?: any) {
    this.news = [];

    this.data.getNewFilter('business').subscribe(
      resp => {

        this.news.push(...resp.articles);

        if (event) {
          event.target.complete();
        }
      }
    )
  }

  loadEntertainmentNews(event?: any) {
    this.news = [];

    this.data.getNewFilter('entertainment').subscribe(
      resp => {

        this.news.push(...resp.articles);

        if (event) {
          event.target.complete();
        }
      }
    )
  }

 

  async lanzarMenu(noti: Article) {
    const actionSheet = await this.actionSheetController.create({
      header: 'Albums',
      cssClass: 'my-custom-class',
      buttons: [{
        text: 'Share',
        icon: 'share',
        handler: () => {
          console.log('Share clicked');
        }
      }, {
        text: 'Favorite',
        icon: 'heart',
        handler: () => {
          this.datalocal.guardarNew(noti);
          console.log('Favorite clicked');
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }
}
