import { Component } from '@angular/core';
import { DataService } from '../../services/data.service';
import { Article } from '../../interfaces/interfaces';
import { ActionSheetController } from '@ionic/angular';

//import { Component, OnInit } from '@angular/core';
import { Plugins } from '@capacitor/core';
import { DataLocalService } from '../../services/data-local.service';
const { Browser } = Plugins;

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  constructor(private dataLocal: DataLocalService, private data: DataService, public actionSheetController: ActionSheetController) {
    this.loadNews();
  }

  news: Array<Article> = [];

  loadNews(event?: any) {
    this.data.getNews().subscribe(
      resp => {
        console.log('News', resp);

        this.news.push(...resp.articles);

        if (event) {
          event.target.complete();
        }
      }
    )
  }

  loadData(event) {
    this.loadNews(event);
  }

  ngOnInit(): void {
    this.loadNews();
  }



  async openBrowser(link: string) {
    // On iOS, for example, open the URL in SFSafariViewController (the in-app browser)
    await Browser.open({ url: link });
  }





  async lanzarMenu(noti: Article) {
    const actionSheet = await this.actionSheetController.create({
      header: 'Albums',
      cssClass: 'my-custom-class',
      buttons: [{
        text: 'Share',
        icon: 'share',
        handler: () => {
          console.log('Share clicked');
        }
      }, {
        text: 'Favorite',
        icon: 'heart',
        handler: () => {
          this.dataLocal.guardarNew(noti);
          console.log('Favorite clicked');
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

}
