import { Component } from '@angular/core';
import { ActionSheetController } from '@ionic/angular';
import { DataLocalService } from '../../services/data-local.service';
import { Article } from '../../interfaces/interfaces';
import { Storage } from '@ionic/storage-angular';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  favNews: Article[] = [];


  constructor(private dataLocal: DataLocalService, public actionSheetController: ActionSheetController, private storage: Storage) {
    this.dataLocal.cargarFavs();
    this.favNews = this.dataLocal.favNews;
  }


  async lanzarMenu(noti: Article) {
    const actionSheet = await this.actionSheetController.create({
      header: 'Albums',
      cssClass: 'my-custom-class',
      buttons: [{
        text: 'Share',
        icon: 'share',
        handler: () => {
          console.log('Share clicked');
        }
      }, {
        text: 'NOT Favorite',
        icon: 'heart',
        handler: () => {
          this.dataLocal.borrarNew(noti);
          this.favNews = this.dataLocal.favNews;
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

  ionViewDidEnter() {
    this.dataLocal.cargarFavs();
    this.favNews = this.dataLocal.favNews;
 }
}
